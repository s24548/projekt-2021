Mrówki Langtona 

Prosta symulacja tzw. automatu komórkowego mrówki Langtona.  

Symulacja odbywa się na planszy o wymiarach 100 na 100 pól, z których każde może być wypełniona lub pusta.  "Mrówką" możemy nazwać pionek, który znajduje się w jednym z pól, mający określony kierunek, w którym się porusza. Mrówka zachowuje się według następujących zasad:  

jeśli znajduje się na polu pustym to obraca się w lewo (o kąt prosty), wypełnia pole i przechodzi na następne pole; 

jeśli znajduje się na polu wypełnionym to obraca się w prawo (o kąt prosty), czyści pole i przechodzi na następne pole; 

Gdy mrówka przekroczy krawędź planszy, powinna być ona przenoszona pod krawędź z przeciwnej strony (tak jakby plansza “zawijała się”). 

 

Użytkownik ma do dyspozycji trzy mrówki - czerwoną, zieloną i niebieską, które poruszają się kolejno, jedna po drugiej. Na początku, program pokazuje w konsoli pustą planszę oraz losowo umieszczoną, czerwoną mrówkę. Mrówka jest reprezentowana w postaci czerwonego znaku, w zależności od jej kierunku: 

"^" - mrówka kierująca się do góry 

">" - mrówka kierująca się w prawo 

"<" - mrówka kierująca się w lewo 

"v" - mrówka kierująca się w dół 
 

Użytkownik może zmienić położenie czerwonej mrówki używając klawiszy strzałek, obrócić w lewo klawiszem R oraz zatwierdzić jej pozycję klawiszem Enter. PO zatwierdzeniu położenia czerwonej mrówki, w taki sam sposób gracz ustala i zatwierdza pozycję zielonej mrówki, a następnie - niebieskiej. Odróżnia je kolor reprezentującego je znaku. 

 

Po umieszczeniu wszystkich mrówek na planszy, program rozpoczyna symulację, co sekundę wykonując ruch kolejnej mrówki. Pola wypełnione są pokazywane jako mające białe tło, a pola puste – jako mające czarne (domyślne dla konsoli) tło.  

 

Użytkownik ma możliwość zmiany prędkości symulacji klawiszami strzałki w górę (dwukrotne skrócenie czasu pomiędzy kolejnymi ruchami) i w dół (dwukrotne wydłużenie czasu pomiędzy ruchami). Może również wstrzymywać i wznawiać symulację, wciskając klawisz spacji. 
