#include <iostream>
#include <curses.h>
#include <unistd.h>
#include <time.h>

#define MROWKA_GORA '^'
#define MROWKA_PRAWO '>'
#define MROWKA_DOL '<'
#define MROWKA_LEWO 'v'

const int szerokosc = 200; 
const int wysokosc = 50; 
int kratka[szerokosc][wysokosc];
int kierunek;

float czas_sleep=1;

void ncurses_init_colors()
{	
	start_color();
	
	init_pair(1, COLOR_RED, COLOR_BLACK); 
	init_pair(2, COLOR_GREEN, COLOR_BLACK); 
	init_pair(3, COLOR_BLUE, COLOR_BLACK); 
    	init_pair(4, COLOR_WHITE, COLOR_WHITE); 
	init_pair(5, COLOR_BLACK, COLOR_BLACK); 
}

void ncurses_config()
{	
	ncurses_init_colors();
	keypad(stdscr, TRUE);
	timeout(1000);
	noecho();
}

void wyswietl_mrowke(int x, int y, int kolor_mrowki, char pozycja)
{
	char znak[4] = {MROWKA_GORA, MROWKA_PRAWO, MROWKA_LEWO, MROWKA_DOL};
	attron(COLOR_PAIR(kolor_mrowki));
        mvaddch(y, x, znak[pozycja]);
        move(0, 0);
        attroff(COLOR_PAIR(kolor_mrowki));
}

void slad_mrowki(int x, int y)
{
	attron(COLOR_PAIR(4));
        mvaddch(y, x, ' ');
        move(0, 0);
        attroff(COLOR_PAIR(4));        
}

void czysc_pole(int x, int y)
{
	attron(COLOR_PAIR(5));
        mvaddch(y, x, ' ');
        move(0, 0);
        attroff(COLOR_PAIR(5));
}

void skret_lewo(char &kierunek)
{
	kierunek--;
	if (kierunek < 0) {
		kierunek = 3;
	}
}

void ustaw_mrowke(int kolor_mrowki, int &x, int &y, char &pozycja)
{
	int ch;
		do {
			ch = getch();
			switch (ch) {
		        	case KEY_UP:
					czysc_pole(x, y);
					y--;
					if (y < 0) {
		                        	y = wysokosc - 1;
		                        }
					wyswietl_mrowke(x,y,kolor_mrowki,pozycja);
		                        break;
		                case KEY_DOWN:
		                        czysc_pole(x, y);
		                        y++;
		                        if (y > wysokosc - 1) {
		                        	y = 0;
		                        }
					wyswietl_mrowke(x,y,kolor_mrowki,pozycja);
		                        break;
		                case KEY_LEFT:
		                        czysc_pole(x, y);
		                        x--;
		                        if (x < 0) {
		                        	x = szerokosc - 1;
		                        }
					wyswietl_mrowke(x,y,kolor_mrowki,pozycja);
		                        break;
		                case KEY_RIGHT:
		                        czysc_pole(x, y);
		                        x++;
		                        if (x > szerokosc - 1) {
		                        	x = 0;
		                        }		                        
					wyswietl_mrowke(x,y,kolor_mrowki,pozycja);
		                        break;
		                case 'R':
					skret_lewo(pozycja);
					wyswietl_mrowke(x,y,kolor_mrowki,pozycja);
		                        break;
		        }
		        refresh();
		}
		while( ch != '\n');
}

void skret_prawo(char &kierunek) 
{
	kierunek++;
	if (kierunek > 3) {
		kierunek = 0;
	}
} 

void naprzod(int &x, int &y, char &kierunek) 
{
	if (kierunek == 0){
		y--;
	}
	else if (kierunek == 1) {
		x++;
	}
	else if (kierunek == 2) {
		y++;
	}
	else if (kierunek == 3) {
		x--;
	}
	if (x > szerokosc - 1) {
		x = 0;
	}
	else if (x < 0){
		x = szerokosc-1;
	}
	if (y > wysokosc - 1) {
		y = 0;
	}
	else if (y < 0) {
		y = wysokosc-1;
	}
}

int obsluga_czasu(int &wstrzymac, float &czas) 
{
	int ch = 0;
	ch = getch();
	
	if (( ch != ' ' ) && ( ch != KEY_UP ) && ( ch != KEY_DOWN )) {
		return wstrzymac;
	}
	else {
		switch (ch) {
                        case KEY_UP:
                                czas=czas/2;
				timeout(czas*1000);
                                break;
                        case KEY_DOWN:
                                czas=czas*2;
				timeout(czas*1000);
                                break;
                        case ' ':
                                if ( wstrzymac == 0) {
                                        do {
						sleep(czas_sleep);
						ch = getch();
					} while ( ch != ' ' );
				}
                                else {
                                        wstrzymac = 0;
				}
                                break;
                }
		czas_sleep=czas;
		return wstrzymac;
	}
}

auto main () -> int
{
	initscr();
	ncurses_config();
	clear();

	srand ( time(NULL) );
	char znak[4] = {0, 1, 2, 3};
	
	int x_czerwona = rand() % 50;
	int y_czerwona = rand() % 50;
	int losowy = rand() % 4;
	
	char pozycja_czerwonej = znak[losowy];
	wyswietl_mrowke(x_czerwona, y_czerwona, 1,pozycja_czerwonej);
	ustaw_mrowke(1, x_czerwona, y_czerwona, pozycja_czerwonej);
	
	int x_zielona = rand() % 50;
	int y_zielona = rand() % 50;
	losowy = rand() % 4;
	
	char pozycja_zielonej = znak[losowy];
	wyswietl_mrowke(x_zielona, y_zielona, 2,pozycja_zielonej);
	ustaw_mrowke(2, x_zielona, y_zielona, pozycja_zielonej);
	
	int x_niebieska = rand() % 50;
	int y_niebieska = rand() % 50;
	losowy = rand() % 4;
	
	char pozycja_niebieskiej = znak[losowy];
	wyswietl_mrowke(x_niebieska, y_niebieska, 3,pozycja_niebieskiej);
	ustaw_mrowke(3, x_niebieska, y_niebieska, pozycja_niebieskiej);
	
	int state;
	int wstrzymac = 0;

	while(true) { // int runda 
		
			if (obsluga_czasu(wstrzymac,czas_sleep) == 0) {
			
				state = kratka[x_czerwona][y_czerwona];
				
				if (state == 1) { 
				
					skret_prawo(pozycja_czerwonej);
					czysc_pole(x_czerwona, y_czerwona);
					kratka[x_czerwona][y_czerwona] = 0;
					naprzod(x_czerwona, y_czerwona, pozycja_czerwonej);
					wyswietl_mrowke(x_czerwona, y_czerwona, 1,pozycja_czerwonej);				
            			}
				else if (state == 0) {
			 
					skret_lewo(pozycja_czerwonej);
					slad_mrowki(x_czerwona, y_czerwona);
					kratka[x_czerwona][y_czerwona] = 1;
					naprzod(x_czerwona, y_czerwona, pozycja_czerwonej);
					wyswietl_mrowke(x_czerwona, y_czerwona, 1,pozycja_czerwonej);		
				}
			
			refresh();
			sleep(czas_sleep);
			
			}
			
			if (obsluga_czasu(wstrzymac,czas_sleep) == 0) {
			
				state = kratka[x_zielona][y_zielona];
			
				if (state == 1) { 
				
					skret_prawo(pozycja_zielonej);
					czysc_pole(x_zielona, y_zielona);
					kratka[x_zielona][y_zielona] = 0;
					naprzod(x_zielona, y_zielona, pozycja_zielonej);
					wyswietl_mrowke(x_zielona, y_zielona, 2,pozycja_zielonej);
            			}
				else if (state == 0) {
			 
					skret_lewo(pozycja_zielonej);
					slad_mrowki(x_zielona, y_zielona);
					kratka[x_zielona][y_zielona] = 1;
					naprzod(x_zielona, y_zielona, pozycja_zielonej);
					wyswietl_mrowke(x_zielona, y_zielona, 2,pozycja_zielonej);					
				}
			
			refresh();
			sleep(czas_sleep);
			
			}
			
			if (obsluga_czasu(wstrzymac,czas_sleep) == 0) {
			
				state = kratka[x_niebieska][y_niebieska];
			
				if (state == 1) { 
				
					skret_prawo(pozycja_niebieskiej);
					czysc_pole(x_niebieska, y_niebieska);
					kratka[x_niebieska][y_niebieska] = 0;
					naprzod(x_niebieska, y_niebieska, pozycja_niebieskiej);
					wyswietl_mrowke(x_niebieska, y_niebieska, 3,pozycja_niebieskiej);
            			}
				else if (state == 0) {
			 
					skret_lewo(pozycja_niebieskiej);
					slad_mrowki(x_niebieska, y_niebieska);
					kratka[x_niebieska][y_niebieska] = 1;
					naprzod(x_niebieska, y_niebieska, pozycja_niebieskiej);
					wyswietl_mrowke(x_niebieska, y_niebieska, 3,pozycja_niebieskiej);	
				}
			
			refresh();
			sleep(czas_sleep);
				
			}
		}	
	endwin();
	exit(0);
		 
return EXIT_SUCCESS;
}
